from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView

from student.filters import StudentFilters
from student.forms import StudentForm
from student.models import Student


class StudentCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'student/create_student.html'      # calea catre fisierul html unde va fi afisat(randat) formularul
    model = Student         # modelul folosit la generarea formularului (tabela creata in models)
    success_url = reverse_lazy('create-student')       # pagina pe care va fi redirectionat userul daca formularul a fost completat cu succes
    form_class = StudentForm
    permission_required = 'student.add_student'


class StudentListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'student/list_students.html'
    model = Student
    context_object_name = 'all_students'
    permission_required = 'student.view_student'

    def get_context_data(self, **kwargs):
        data = super(StudentListView, self).get_context_data(**kwargs)
        all_students = Student.objects.all()
        my_filter = StudentFilters(self.request.GET, queryset=all_students)
        all_students = my_filter.qs
        data['all_students'] = all_students
        data['my_filter'] = my_filter
        return data


class StudentUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'student/update_student.html'
    model = Student
    success_url = reverse_lazy('list-of-students')
    form_class = StudentForm
    permission_required = 'student.change_student'


class StudentDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'student/delete_student.html'
    model = Student
    success_url = reverse_lazy('list-of-students')
    permission_required = 'student.delete_student'


class StudentDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'student/detail_student.html'
    model = Student
    permission_required = 'student.detail_view'


