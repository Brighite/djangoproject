import django_filters

from student.models import Student


class StudentFilters(django_filters.FilterSet):
    class Meta:
        model = Student
        fields = {
            'first_name': ['icontains'],
            'last_name': ['icontains'],
            'age': ['icontains'],
            'date_of_birth': ['icontains'],
            'olympic': ['icontains'],
            'gender': ['icontains'],
            # 'teacher': ['icontains'],
            'created_at': ['icontains'],
            'updated_at': ['icontains']
        }