from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
# Create your views here.
from django.shortcuts import render
from django.views.generic import TemplateView


def homepage(request):
    return HttpResponse("Hello Brighite, you lovely creature of the Universe !")


@login_required
def home(request):
    context = {
        "all_students": [
            {
            "first_name": "Claudiu",
            "last_name": "Nechifor",
            "age": 31
            },
        {
            "first_name": "Mircea",
            "last_name": "Popovici",
            "age": 32
        }
    ]
    }
    return render(request, "home/home.html", context)


def brands(request):
    context = {
        "cars": [
            {
                "brand_name": "Opel",
                "model_name": "Insignia"
            },
            {
                "brand_name": "Renault",
                "model_name": "Talisman"
            }
        ]
    }
    return render(request, "home/brands.html", context)


class HomeTemplateView(TemplateView):
    template_name = 'home/homepage.html'
