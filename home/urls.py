from django.urls import path

from home import views

urlpatterns = [
    path('page/', views.homepage, name='page'),
    path('all_students/', views.home, name='list_of_students'),
    path('brands/', views.brands, name='brand_names'),
    path('', views.HomeTemplateView.as_view(), name='homepage'),
    # path('list_of_teachers/', views.home, name='list_of_teachers')
]
