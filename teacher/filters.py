import django_filters

from teacher.models import Teacher


class TeacherFilters(django_filters.FilterSet):
    class Meta:
        model = Teacher
        fields = ['first_name', 'last_name', 'specialisation', 'active', 'created_at']

