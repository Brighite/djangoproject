from django.db import models


class Teacher(models.Model):
    spec_choices = (
        ('Primary School', 'Primary School'),
        ('Middle School', 'Middle School'),
        ('High School', 'High School')
    )
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    specialisation = models.CharField(max_length=30, choices=spec_choices)
    active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
