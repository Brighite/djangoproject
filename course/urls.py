from django.urls import path

from course import views

urlpatterns = [
    path('teacher_course/<int:teacher_id>/', views.get_all_teachers_per_course, name='teacher-per-course')
]
